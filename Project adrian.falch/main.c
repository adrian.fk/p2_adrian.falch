
/************************************************************************************************
 * @Title JewelHunt
 * @objective The objective of this program is both to present the user a functional version
 * of the game Magic Jewelry (Hence the title) and to store both the desired gameplay from input
 * of the user, and also all sort and store a rankings list of this particular game.
 *
 * @author Adiran Falch
 * @email adrian.falch@students.salle.url.edu
 * @Creation date: 14.03.19
 * @Last modification date: 31.03.19
 *
************************************************************************************************/

#include <stdio.h>
#include "LS_allegro.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

// PRE-PROCESS MACROS
#define MENU_EXIT_STRING "Exit"
#define INPUT_SIZE 100
#define FALSE 0
#define TRUE 1
#define LINE_ENDING '\n'
#define NULL_CHAR '\0'

//MENU STATE PPM (Pre-process macros)
#define STATE_MAIN_MENU 1
#define STATE_PLAY_MENU 2
#define STATE_SHOW_RANKINGS 3
#define STATE_EXIT 4

//GAME CONSTANTS
#define SCREEN_HEIGHT_PIXELS 800
#define SCREEN_WIDTH_PIXELS 720
#define MAP_HEIGHT_SQUARES 18
#define MAP_WIDTH_SQUARES 10
#define WINDOW_HEIGHT_SQUARES 22
#define WINDOW_WIDTH_SQUARES 20
#define DATA_SEPERATOR "-"

#define MAX_BALL_SPAWN 3
#define FIGURE_NONE 0
#define FIGURE_CIRCLE 1
#define FIGURE_TRIANGLE 2
#define FIGURE_DIAMOND 3
#define FIGURE_STAR 4

#define FIGURE_RECTANGLE 5
#define FIGURE_SPAWN_1 0
#define FIGURE_SPAWN_2 1
#define FIGURE_SPAWN_3 2
#define RANKINGS_SIZE 100



typedef struct Strings {
    int length;
    char string[INPUT_SIZE];
} String;

typedef struct {
    float current;
    float startpoint;
    float endpoint;
}Coordinate;


typedef struct {
    Coordinate x;
    Coordinate y;
}Pixelspace;

typedef struct {
    int seconds;
    int minutes;
    int hours;
}Time;

typedef struct {
    int active;
    Pixelspace pixels;
    int shape;
}Boxes;

typedef struct {
    String usernames[RANKINGS_SIZE];
    int scores[RANKINGS_SIZE];
}Ranking;

struct Games {
    Boxes game_map[MAP_WIDTH_SQUARES][MAP_HEIGHT_SQUARES];
    Boxes game_window[WINDOW_WIDTH_SQUARES][WINDOW_HEIGHT_SQUARES];
    Boxes next_figures[MAX_BALL_SPAWN];
    float box_height;
    float box_width;
    String username;
    int gameOver;
    int points;
    int focused_x[MAX_BALL_SPAWN];
    int focused_y[MAX_BALL_SPAWN];
    Time time;
    Ranking ranks;
};

/*******************************************************************
 * @objectives this function counts every character of the string
 * input until first '\0' or '\n'
 * and returns that value re
 *
 * @param input[], length
 * @return length
 */
int stringLength(const char input[]) {
    int i;
    int length = 0;

    /*looping through string and counting array members until line ending or null is reached*/
    for (i = 0; input[i] != NULL_CHAR && input[i] != LINE_ENDING; i++) {
        length++;
    }
    return length;
}

/*******************************************************************
 * @objectives This procedure checks if the rankings file exists
 * if not, it establishes one for later appending of scores.
 * @param fp
 */
void checkRankingsFile() {
    FILE *fp;
    fp = fopen("rankings.txt", "r");
    if (fp == NULL) {
        fp = fopen("rankings.txt", "w");
        fclose(fp);
    }
    else {
        fclose(fp);
    }

}

/*******************************************************************
 * @objectives This procedure appends the current rankings of the
 * current player.
 * @param *game.
 * @return
 */
void saveRankings(struct Games *game) {
    FILE *f;
    f = fopen("rankings.txt", "a");
    if (NULL == f) {
        checkRankingsFile();
    }
    else {
        fprintf(f, "%s", game->username.string);
        fprintf(f, DATA_SEPERATOR);
        fprintf(f, "%d", game->points);
        fprintf(f, "\n");
        fclose(f);
    }
}

/*******************************************************************
 * @objectives This function first locates the next empty slot in
 * our rankings arrays, then later inserts the segmented data
 * found from the text file loaded where this function is called.
 * @param username, score
 * @return
 */
void updateRankings(struct Games *game, String username, int score) {
    int i, e;
/*Locating first empty spot in the array and filling it with the loaded username and score*/
    for (i = 0; i < RANKINGS_SIZE; i++) {
        if (game->ranks.usernames[i].string[0] == NULL_CHAR) {
            for (e = 0; e < INPUT_SIZE; e++) {
                game->ranks.usernames[i].string[e] = username.string[e];
            }
            game->ranks.scores[i] = score;
            i = RANKINGS_SIZE;
        }
    }
}

/*******************************************************************
 * @objectives This procedure serves the purpose of removing the entry
 * at array position "pos" from the array lists representing the different
 * users and their respective scores aka. the rankings.
 *
 * @param *game, pos
 */
void removeRankEntry (struct Games *game, int pos) {
    int i, e;
    for (i = pos +1; i < RANKINGS_SIZE; i++) {
        if (i != 0) {
            for (e = 0; e < stringLength(game->ranks.usernames[i].string); e++) {
                game->ranks.usernames[i - 1].string[e] = game->ranks.usernames[i].string[e];
            }
            game->ranks.scores[i - 1] = game->ranks.scores[i];
        }
    }
    for (i = 0; i < RANKINGS_SIZE; i++) {
        if (game->ranks.scores[i] == 0) {
            for (e = 0; e < INPUT_SIZE; e++) {
                game->ranks.usernames[i].string[e] = '\0';
            }
        }
    }
}

/*******************************************************************
 * @objectives This procedure looks for a username multiply stored in
 * its representative array.
 *
 * @param *game,
 * @return
 */
void checkRankingsDuplicate(struct Games *game) {
    int i, e;
    /*Firstly looping through whole rankings list, for every iteration we loop through whole array again to look for multiply*/
    for (i = 0; i < RANKINGS_SIZE; i++) {
        for (e = 0; e < RANKINGS_SIZE; e++) {
            if (e == i) {
                //do nothing
            }
            else {

                if (strcmp(game->ranks.usernames[i].string, game->ranks.usernames[e].string) == 0) {
                    if (game->ranks.scores[i] >= game->ranks.scores[e]) {
                        removeRankEntry(game, e);

                    } else {
                        game->ranks.scores[i] = game->ranks.scores[e];
                        removeRankEntry(game, e);
                    }
                }

            }
        }
    }
}

/*******************************************************************
 * @objectives This procedure serves the purpose of sorting the rankings
 * from biggest to smallest.
 * @param *game, username, score, tmpRanks, sortedRanks, array_pos_biggest
 */
void sortRankings(struct Games *game) {
    int i, e, j;
    Ranking tmp_ranks;
    Ranking sorted_ranks;
    String username;
    int score = 0;
    int array_pos_biggest = 0;


    //initialization of new ranking structs
    for (i = 0; i < RANKINGS_SIZE; i++) {
        for (e = 0; e < INPUT_SIZE; e++) {
            tmp_ranks.usernames[i].string[e] = '\0';
        }
        for (e = 0; e < INPUT_SIZE; e++) {
            sorted_ranks.usernames[i].string[e] = '\0';
        }
        tmp_ranks.scores[i] = 0;
        sorted_ranks.scores[i] = 0;
    }
    //load game->ranks into tmp_ranks
    for (i = 0; i < RANKINGS_SIZE; i++) {
        tmp_ranks.usernames[i] = game->ranks.usernames[i];
        tmp_ranks.scores[i] = game->ranks.scores[i];
    }

    //Cleaning original array

    for (i = 0; i < RANKINGS_SIZE; i++) {
        for (j = 0; j < INPUT_SIZE; j++) {
            game->ranks.usernames[i].string[j] = '\0';
        }
        game->ranks.scores[i] = 0;
    }


    //Looping through all array locations sorting the arrays in correct chronological order
    for (j = 0; j < RANKINGS_SIZE; j++) {

        //Reseting values to get ready for next biggest value
        for (e = 0; e < RANKINGS_SIZE; e++) {
            username.string[e] = '\0';
        }
        array_pos_biggest = 0;
        score = 0;

        //find biggest
        for (i = 0; i < RANKINGS_SIZE; i++) {
            if (tmp_ranks.scores[i] > score) { //Rules out scores of 0

                //Handle bigger score found
                //Noting array pos found
                array_pos_biggest = i;
                //setting username to biggest user found
                username = tmp_ranks.usernames[array_pos_biggest];

                //setting biggest score to biggest found
                score = tmp_ranks.scores[array_pos_biggest];
            }
        }
        //place biggest in top of sorted_ranks
        for (i = 0; i < RANKINGS_SIZE; i++) {
            if (sorted_ranks.usernames[i].string[0] == NULL_CHAR) {

                sorted_ranks.usernames[i] = tmp_ranks.usernames[array_pos_biggest];
                sorted_ranks.scores[i] = score;
                i = RANKINGS_SIZE;
            }
        }
        //Remove, from tmp_ranks, the "biggest" rank already distributed
        for (e = 0; e < strlen(tmp_ranks.usernames[array_pos_biggest].string); e++) {
            tmp_ranks.usernames[array_pos_biggest].string[e] = '\0';
        }
        tmp_ranks.scores[array_pos_biggest] = 0;
    }

    //re-load the now sorted rankings back into main game rankings struct
    for (i = 0; i < RANKINGS_SIZE; i++) {
        game->ranks.usernames[i] = sorted_ranks.usernames[i];
        game->ranks.scores[i] = sorted_ranks.scores[i];
    }

}

/*******************************************************************
 * @objectives: This procedure serves the purpose of saving the rankings
 * from the two ranking arrays stored inside the main struct "game"
 * @param *game, f
 * @return
 */
void saveUpdatedRankings(struct Games *game) {
    int i, e;               //Triggers
    FILE *f;                //File pointer
    f = fopen("rankings.txt", "w");
    if (NULL == f) {
        checkRankingsFile();
    }
    else {
        for (i = 0; i < RANKINGS_SIZE; i++) {
            if (game->ranks.usernames[i].string[0] != '\0') {
                fprintf(f, "%s", game->ranks.usernames[i].string);
                fprintf(f, DATA_SEPERATOR);
                fprintf(f, "%d", game->ranks.scores[i]);
                fprintf(f, "\n");
            }
        }

        fclose(f);
    }
}

/*******************************************************************
 * @objectives This function prints in the console the rankings
 * @param *game
 */
void printConsoleRankings(struct Games *game) {
    int i;
    for (i = 0; i < RANKINGS_SIZE; i++) {
        if (game->ranks.usernames[i].string[0] != '\0') {
            printf("\n%d. %s  :  %d", (i+1), game->ranks.usernames[i].string, game->ranks.scores[i]);
        }
    }
    printf("\n\n"); //Formatting
}

/*******************************************************************
 * @objectives This function serves the purpose of segmenting the
 * text file containing the rankings, later to call for some
 * different procedures to sort the rankings from highest score
 * to lowest.
 * @param *game, dataType, int_score, f
 */
void loadRankings(struct Games *game) {
    int i, e, j;                        // Triggers
    FILE *f;                            // File pointer
    String input, username, score;      // Temporary string storage variables
    int dataType = 0;                   // Datatype input handler
    int int_score = 0;


    for (e = 0; e < INPUT_SIZE; e++) {
        input.string[e] = '\0';
    }
    for (e = 0; e < INPUT_SIZE; e++) {
        score.string[e] = '\0';
    }
    //Initiating storage arrays
    for (i = 0; i < RANKINGS_SIZE; i++) {
        for (j = 0; j < INPUT_SIZE; j++) {
            game->ranks.usernames[i].string[j] = '\0';
        }
        game->ranks.scores[i] = 0;
    }

    f = fopen("rankings.txt", "r");
    if (NULL == f) {
        saveRankings(game);
    }
    else {
        while (!feof(f)) {
            for (e = 0; e < INPUT_SIZE; e++) {
                username.string[e] = '\0';
            }
            e = 0;
            fgets(input.string, 150, f);
            int_score = 0;

            for (j = 0; j < INPUT_SIZE; j++) {
                score.string[j] = '\0';
            }

            for (i = 0; input.string[i] != '\n' && input.string[i] != '\0'; i++) {

                if (input.string[i] == '-') {
                    dataType = 1;
                    e = 0;
                }
                else {
                    switch (dataType) {
                        case 0:
                            //Cut out username
                            username.string[e] = input.string[i];
                            e++;
                            break;

                        case 1:
                            score.string[e] = input.string[i];
                            e++;
                            break;

                        default:
                            break;
                    }
                }
                // Switch back to dataType 0
                if (i == stringLength(input.string) - 1) {
                    dataType = 0;
                    e = 0;
                }
            }
            username.length = stringLength(username.string);
            int_score = atoi(score.string);
            updateRankings(game, username, int_score);
        }
        fclose(f);

        checkRankingsDuplicate(game);
        sortRankings(game);
        saveUpdatedRankings(game);
    }
}

/*******************************************************************
 * @objectives This function is the main function in terms of handling
 * the printing of the game-window's supportive graphics, such as
 * Time, Score, Username, Bord limitations, with more.
 *
 * @param *game
 */
void printWindowGraphics(struct Games *game) {
    int i, j;
    int next_jewels_y[MAX_BALL_SPAWN], next_jewels_x[MAX_BALL_SPAWN];
    game->box_height = ((float) SCREEN_HEIGHT_PIXELS / (float) WINDOW_HEIGHT_SQUARES);
    game->box_width = ((float) SCREEN_WIDTH_PIXELS / (float) WINDOW_WIDTH_SQUARES);

    //Setting which boxes to fill for the column that will be filled from top to bottom

    for (i = 0; i < WINDOW_WIDTH_SQUARES; i++) {
        for (j = 0; j < WINDOW_HEIGHT_SQUARES; j++) {
            if (i == 0) {
                game->game_window[i][j].active = TRUE;
                game->game_window[i][j].shape = FIGURE_NONE;
            }
            if (i > MAP_WIDTH_SQUARES) {
                game->game_window[i][j].active = TRUE;
                game->game_window[i][j].shape = FIGURE_NONE;
            }
            if ((i > 0 && i <= MAP_WIDTH_SQUARES) || (i > (MAP_WIDTH_SQUARES + 2) && i <= WINDOW_WIDTH_SQUARES -2)) {
                if (j < 3 || j > MAP_HEIGHT_SQUARES) {
                    game->game_window[i][j].active = TRUE;
                    game->game_window[i][j].shape = FIGURE_NONE;
                }
                else {
                    game->game_window[i][j].active = FALSE;
                    game->game_window[i][j].shape = FALSE;
                    
                }
            }
        }
    }
    //Next balls menu
    next_jewels_x[0] = WINDOW_WIDTH_SQUARES - 5; next_jewels_x[1] = WINDOW_WIDTH_SQUARES - 5; next_jewels_x[2] = WINDOW_WIDTH_SQUARES - 5;
    next_jewels_y[0] = WINDOW_HEIGHT_SQUARES/2 - (MAP_WIDTH_SQUARES / 2 - 2) - 4;
    next_jewels_y[1] = WINDOW_HEIGHT_SQUARES/2 - (MAP_WIDTH_SQUARES / 2 - 2) - 3;
    next_jewels_y[2] = WINDOW_HEIGHT_SQUARES/2  - (MAP_WIDTH_SQUARES / 2 - 2)-2;

    game->game_window[next_jewels_x[0]][next_jewels_y[0]].shape = game->next_figures[0].shape;
    game->game_window[next_jewels_x[1]][next_jewels_y[1]].shape = game->next_figures[1].shape;
    game->game_window[next_jewels_x[2]][next_jewels_y[2]].shape = game->next_figures[2].shape;

    game->game_window[next_jewels_x[0]][next_jewels_y[0]].active = TRUE;
    game->game_window[next_jewels_x[1]][next_jewels_y[1]].active = TRUE;
    game->game_window[next_jewels_x[2]][next_jewels_y[2]].active = TRUE;


    //Filling boxes marked Active = TRUE
    for (i = 0; i < WINDOW_WIDTH_SQUARES; i++) {
        for (j = 0; j < WINDOW_HEIGHT_SQUARES; j++) {
            game->game_window[i][j].pixels.x.startpoint = i * game->box_width + 2;
            game->game_window[i][j].pixels.x.endpoint = (i + 1) * game->box_width + 2;
            game->game_window[i][j].pixels.y.startpoint = j * game->box_height;
            game->game_window[i][j].pixels.y.endpoint = (j + 1) * game->box_height;

            //Next balls background
            if ((i == next_jewels_x[0] || i == next_jewels_x[1] || i == next_jewels_x[2]) && (j == next_jewels_y[0] || j == next_jewels_y[1] || j == next_jewels_y[2])) {
            }
            else {
                if (TRUE == game->game_window[i][j].active) {
                    al_draw_filled_rectangle(game->game_window[i][j].pixels.x.startpoint,
                                             game->game_window[i][j].pixels.y.startpoint,
                                             game->game_window[i][j].pixels.x.endpoint,
                                             game->game_window[i][j].pixels.y.endpoint,
                                             LS_allegro_get_color(LIGHT_GRAY));
                    al_draw_rectangle(game->game_window[i][j].pixels.x.startpoint,
                                      game->game_window[i][j].pixels.y.startpoint,
                                      game->game_window[i][j].pixels.x.endpoint,
                                      game->game_window[i][j].pixels.y.endpoint,
                                      al_map_rgb(62, 62, 62), 1);
                }
            }


            //Username box background
            if (j == (WINDOW_HEIGHT_SQUARES - 2) && i >= 3 && i <= 8) {
                al_draw_filled_rectangle(game->game_window[i][j].pixels.x.startpoint -1,
                                         game->game_window[i][j].pixels.y.startpoint,
                                         game->game_window[i][j].pixels.x.endpoint -1,
                                         game->game_window[i][j].pixels.y.endpoint -1,
                                         LS_allegro_get_color(BLACK));
            }
            else {
                //draw dark gray lines where lines shall be (Not in username box)
                if (FALSE == game->game_window[i][j].active) {
                    if (i <= MAP_WIDTH_SQUARES) {
                        al_draw_rectangle(game->game_window[i][j].pixels.x.startpoint,
                                          game->game_window[i][j].pixels.y.startpoint,
                                          game->game_window[i][j].pixels.x.endpoint,
                                          game->game_window[i][j].pixels.y.endpoint,
                                          al_map_rgb(22, 22, 22), 0); // 22 22 22 = Dark gray
                    }
                }
            }

            if (TRUE == game->game_window[i][j].active) {
                // Window functionality for next balls
                //Rectangle
                if (game->game_window[i][j].shape == FIGURE_RECTANGLE) {
                    game->game_window[i][j].pixels.x.startpoint = i * game->box_width;
                    game->game_window[i][j].pixels.x.endpoint = ((i + 1) * game->box_width);
                    game->game_window[i][j].pixels.y.startpoint = (j * game->box_height) + 4;
                    game->game_window[i][j].pixels.y.endpoint = ((j + 1) * game->box_height) - 4;
                    al_draw_filled_rectangle(game->game_window[i][j].pixels.x.startpoint,
                                             game->game_window[i][j].pixels.y.startpoint,
                                             game->game_window[i][j].pixels.x.endpoint,
                                             game->game_window[i][j].pixels.y.endpoint,
                                             LS_allegro_get_color(BLUE));
                }


                // Circle
                if (game->game_window[i][j].shape == FIGURE_CIRCLE) {
                    game->game_window[i][j].pixels.x.startpoint = i * game->box_width + (game->box_width / (float) 2);
                    game->game_window[i][j].pixels.y.startpoint =
                            j * game->box_height + (game->box_height / (float) 2) + 4;
                    al_draw_filled_circle(game->game_window[i][j].pixels.x.startpoint,
                                          game->game_window[i][j].pixels.y.startpoint,
                                          (game->box_height / (float) 2) - 3,
                                          LS_allegro_get_color(RED));
                }
                // Triangle

                if (game->game_window[i][j].shape == FIGURE_TRIANGLE) {
                    game->game_window[i][j].pixels.x.startpoint = i * game->box_width + (game->box_width / (float) 2);
                    game->game_window[i][j].pixels.y.startpoint = j * game->box_height;
                    al_draw_filled_triangle(game->game_window[i][j].pixels.x.startpoint,//first vertex
                                            game->game_window[i][j].pixels.y.startpoint,//first vertex
                                            game->game_window[i][j].pixels.x.startpoint -
                                            (game->box_width / (float) 2),//secound vertex
                                            game->game_window[i][j].pixels.y.startpoint +
                                            game->box_height,// secound vertex
                                            game->game_window[i][j].pixels.x.startpoint +
                                            (game->box_width / (float) 2),//third vertex
                                            game->game_window[i][j].pixels.y.startpoint +
                                            game->box_height,// secound third
                                            LS_allegro_get_color(GREEN)); //color
                }
                //Diamond
                if (game->game_window[i][j].shape == FIGURE_DIAMOND) {
                    game->game_window[i][j].pixels.x.startpoint =
                            i * game->box_width + (game->box_width / 2) - 8;
                    game->game_window[i][j].pixels.x.endpoint =
                            ((i + 1) * game->box_width) - 4 - (game->box_width / 2) + 8;
                    game->game_window[i][j].pixels.y.startpoint = (j * game->box_height) + 2;
                    game->game_window[i][j].pixels.y.endpoint = ((j + 1) * game->box_height) - 2;
                    al_draw_filled_rectangle(game->game_window[i][j].pixels.x.startpoint,
                                             game->game_window[i][j].pixels.y.startpoint,
                                             game->game_window[i][j].pixels.x.endpoint,
                                             game->game_window[i][j].pixels.y.endpoint,
                                             LS_allegro_get_color(PINK)); // color
                }
                //Star
                if (game->game_window[i][j].shape == FIGURE_STAR) {
                    game->game_window[i][j].pixels.x.startpoint = i * game->box_width + (game->box_width / (float) 2);
                    game->game_window[i][j].pixels.y.startpoint = (j + 1) * game->box_height + 2;
                    al_draw_filled_triangle(game->game_window[i][j].pixels.x.startpoint,//first vertex
                                            game->game_window[i][j].pixels.y.startpoint,//first vertex
                                            game->game_window[i][j].pixels.x.startpoint -
                                            (game->box_width / (float) 2),//secound vertex
                                            game->game_window[i][j].pixels.y.startpoint -
                                            game->box_height,// secound vertex
                                            game->game_window[i][j].pixels.x.startpoint +
                                            (game->box_width / (float) 2),//third vertex
                                            game->game_window[i][j].pixels.y.startpoint -
                                            game->box_height,// secound third
                                            LS_allegro_get_color(YELLOW)); //color
                }
            }
        }
    }
    
    //UI
    //Next balls - text
    al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), ((MAP_WIDTH_SQUARES + 5) * game->box_width) + game->box_width/2
    ,
                    (3*game->box_height) + game->box_width/2,ALLEGRO_ALIGN_CENTER,"%s", "Next jewels:");    
    //Username
    al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), (game->box_width + ((MAP_WIDTH_SQUARES*game->box_width)/2)),(SCREEN_HEIGHT_PIXELS -
                    (2* game->box_height) + game->box_height/2),ALLEGRO_ALIGN_CENTER,"%s",game->username.string);
    //Score
    al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), SCREEN_WIDTH_PIXELS - (5*game->box_width),
                    SCREEN_HEIGHT_PIXELS - (7*game->box_height),ALLEGRO_ALIGN_CENTER,"%s", "SCORE:");                
    al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), SCREEN_WIDTH_PIXELS - (5*game->box_width),
                    SCREEN_HEIGHT_PIXELS - (6*game->box_height),ALLEGRO_ALIGN_CENTER,"%d", game->points);
    //Time
    al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), SCREEN_WIDTH_PIXELS - (5*game->box_width),
                    SCREEN_HEIGHT_PIXELS - (12*game->box_height),ALLEGRO_ALIGN_CENTER,"%s", "TIME:");

    al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), SCREEN_WIDTH_PIXELS - (2*game->box_width),
                  SCREEN_HEIGHT_PIXELS - (11*game->box_height),ALLEGRO_ALIGN_CENTER,"%d%s", game->time.seconds, "s");

    al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), SCREEN_WIDTH_PIXELS - (3*game->box_width),
                  SCREEN_HEIGHT_PIXELS - (11*game->box_height),ALLEGRO_ALIGN_CENTER,"%d%s", game->time.minutes,"m");

    al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), SCREEN_WIDTH_PIXELS - (4*game->box_width),
                  SCREEN_HEIGHT_PIXELS - (11*game->box_height),ALLEGRO_ALIGN_CENTER,"%d%s", game->time.hours, "h");
                    
} // End function printWindowGraphics

/*******************************************************************
 * @objectives This function is the core of the handling of displaying
 * the different interactive elements in the actual game.
 * Such as; the static elements (jeweles), the moving elements,
 * generally also refered to as "the gameboard"
 *
 * @param *game, i (x coordinate of our matrice), j (y coordinates)
 */
void printGameGraphics(struct Games *game) {
    int i, j;
    float offset_x = game->box_width;
    float offset_y = (3 * game->box_height);
    for (i = 0; i < MAP_WIDTH_SQUARES; i++) {
        for (j = 0; j < MAP_HEIGHT_SQUARES; j++) {
            if (FALSE == game->game_map[i][j].active) {
                game->game_map[i][j].shape = FIGURE_NONE;
            }
        }
    }

    //Filling boxes marked Active = TRUE
    for (i = 0; i < MAP_WIDTH_SQUARES; i++) {
        for (j = 0; j < MAP_HEIGHT_SQUARES; j++) {
            if (TRUE == game->game_map[i][j].active) {


                //Rectangle
                if (game->game_map[i][j].shape == FIGURE_RECTANGLE) {
                    game->game_map[i][j].pixels.x.startpoint = i * game->box_width + offset_x;
                    game->game_map[i][j].pixels.x.endpoint = ((i + 1) * game->box_width) + offset_x;
                    game->game_map[i][j].pixels.y.startpoint = (j * game->box_height) + offset_y + 4;
                    game->game_map[i][j].pixels.y.endpoint = ((j + 1) * game->box_height) + offset_y - 4;
                    al_draw_filled_rectangle(game->game_map[i][j].pixels.x.startpoint,
                                             game->game_map[i][j].pixels.y.startpoint,
                                             game->game_map[i][j].pixels.x.endpoint,
                                             game->game_map[i][j].pixels.y.endpoint,
                                             LS_allegro_get_color(BLUE));
                }


                // Circle
                if (game->game_map[i][j].shape == FIGURE_CIRCLE) {
                    game->game_map[i][j].pixels.x.startpoint = i * game->box_width+ (game->box_width/(float)2) + offset_x;
                    game->game_map[i][j].pixels.y.startpoint = j * game->box_height + (game->box_height/(float)2) + offset_y + 4;
                    al_draw_filled_circle(game->game_map[i][j].pixels.x.startpoint,
                                             game->game_map[i][j].pixels.y.startpoint, (game->box_height/(float)2)-3,
                                             LS_allegro_get_color(RED));
                }
                // Triangle

                if (game->game_map[i][j].shape == FIGURE_TRIANGLE) {
                    game->game_map[i][j].pixels.x.startpoint = i * game->box_width+ (game->box_width/(float)2) + offset_x;
                    game->game_map[i][j].pixels.y.startpoint = j * game->box_height + offset_y;
                    al_draw_filled_triangle(game->game_map[i][j].pixels.x.startpoint,//first vertex
                            game->game_map[i][j].pixels.y.startpoint,//first vertex
                            game->game_map[i][j].pixels.x.startpoint - (game->box_width/(float) 2),//secound vertex
                            game->game_map[i][j].pixels.y.startpoint + game->box_height,// secound vertex
                            game->game_map[i][j].pixels.x.startpoint + (game->box_width/(float) 2),//third vertex
                            game->game_map[i][j].pixels.y.startpoint + game->box_height,// secound third
                            LS_allegro_get_color(GREEN)); //color
                }
                //Diamond
                if (game->game_map[i][j].shape == FIGURE_DIAMOND) {
                    game->game_map[i][j].pixels.x.startpoint =
                            i * game->box_width + offset_x + (game->box_width / 2) - 8;
                    game->game_map[i][j].pixels.x.endpoint =
                            ((i + 1) * game->box_width) + offset_x - 4 - (game->box_width / 2) + 8;
                    game->game_map[i][j].pixels.y.startpoint = (j * game->box_height) + offset_y + 2;
                    game->game_map[i][j].pixels.y.endpoint = ((j + 1) * game->box_height) + offset_y - 2;
                    al_draw_filled_rectangle(game->game_map[i][j].pixels.x.startpoint,
                                             game->game_map[i][j].pixels.y.startpoint,
                                             game->game_map[i][j].pixels.x.endpoint,
                                             game->game_map[i][j].pixels.y.endpoint,
                                             LS_allegro_get_color(PINK)); // color
                }
                //Star
                if (game->game_map[i][j].shape == FIGURE_STAR) {
                    game->game_map[i][j].pixels.x.startpoint = i * game->box_width + (game->box_width/(float)2) + offset_x;
                    game->game_map[i][j].pixels.y.startpoint = (j + 1) * game->box_height + 2 + offset_y;
                    al_draw_filled_triangle(game->game_map[i][j].pixels.x.startpoint,//first vertex
                                            game->game_map[i][j].pixels.y.startpoint,//first vertex
                                            game->game_map[i][j].pixels.x.startpoint - (game->box_width/(float) 2),//secound vertex
                                            game->game_map[i][j].pixels.y.startpoint - game->box_height,// secound vertex
                                            game->game_map[i][j].pixels.x.startpoint + (game->box_width/(float) 2),//third vertex
                                            game->game_map[i][j].pixels.y.startpoint - game->box_height,// secound third
                                            LS_allegro_get_color(YELLOW)); //color
                }
            }
        }

    }
}

/*******************************************************************
 * @objectives This function deals with the randomizing of the values
 * used to generate a next set of random values.
 *
 * @return result
 */
int randomizeSpawn() {
    int result;
    result = ((rand() % 100 + 1) /10) / 2;
    if (0 == result) {
        result = FIGURE_RECTANGLE;
    }
    return result;
}

/*******************************************************************
 * @objectives this procedure deals with setting all the operating
 * values of the gameboard matrice such that it spawns some new jewels
 * and setting the moving element to those new jewels
 *
 * @param
 */
void spawnFigures(struct Games *game) {
    int i;
    //set game.focused array to spawn boxes
    game->focused_x[0] = MAP_WIDTH_SQUARES/2;                   //X spawn
    game->focused_x[1] = MAP_WIDTH_SQUARES/2;
    game->focused_x[2] = MAP_WIDTH_SQUARES/2;
    game->focused_y[0] = FIGURE_SPAWN_1;                        //Y spawn
    game->focused_y[1] = FIGURE_SPAWN_2;
    game->focused_y[2] = FIGURE_SPAWN_3;

    game->game_map[game->focused_x[0]][game->focused_y[0]].active = TRUE;
    game->game_map[game->focused_x[1]][game->focused_y[1]].active = TRUE;
    game->game_map[game->focused_x[2]][game->focused_y[2]].active = TRUE;

    game->game_map[game->focused_x[0]][game->focused_y[0]].shape = game->next_figures[0].shape;
    game->game_map[game->focused_x[1]][game->focused_y[1]].shape = game->next_figures[1].shape;
    game->game_map[game->focused_x[2]][game->focused_y[2]].shape = game->next_figures[2].shape;

    game->next_figures[0].shape = randomizeSpawn();
    game->next_figures[1].shape = randomizeSpawn();
    game->next_figures[2].shape = randomizeSpawn();



    // Randomizing next set of figures
    for (i = 0; i < MAX_BALL_SPAWN;i++){
        game->next_figures[i].shape = randomizeSpawn();
        if (game->next_figures[i].shape == game->game_map[game->focused_x[i]][game->focused_y[i]].shape) {
            i--;
        }
    }
}

/*******************************************************************
 * @objectives This function's purpose is to move the different columns
 * required to moved down, down once for each figure participating in
 * the horizontal link found.
 *
 * @param i (x coordinate of value found linking horizontally), j (y coordinate)
 */
void moveHorizontalDown(struct Games *game, int i, int j) {
    int e, k;
    for (k = MAX_BALL_SPAWN - 1; k >= 0; k--) {
        for (e = MAP_HEIGHT_SQUARES - 1; e >= 0; e--) {
            if (e <= j) {
                game->game_map[i + k][e].shape = game->game_map[i + k][e - 1].shape;
                game->game_map[i + k][e].active = game->game_map[i + k][e - 1].active;


            }
        }

    }
}

/*******************************************************************
 * @objectives Move column down one square at the value of i and j
 * and upwards.
 *
 */
void shiftOneDown(struct Games *game, int i, int j) {
    int e;

    for (e = j; e >= 0; e--) {
        if (e <= j) {
            game->game_map[i][e].shape = game->game_map[i][e - 1].shape;
            game->game_map[i][e].active = game->game_map[i][e - 1].active;
        }
    }
}

/*******************************************************************
 * @objectives This procedure handles the checking of linking
 * figures (Jewels).
 */
void checkPoints(struct Games *game) {
    int i, j, k;

    for (i = 0; i < MAP_WIDTH_SQUARES; i++) {
        for (j = 0; j < MAP_HEIGHT_SQUARES; j++) {
            //horizontal
            if (((game->game_map[i][j].shape == game->game_map[i + 1][j].shape &&
                  game->game_map[i][j].shape == game->game_map[i + 2][j].shape)) &&
                (game->game_map[i][j].active == TRUE)) {
                //Moving down
                moveHorizontalDown(game, i, j);

                //And giving points
                game->points++;
            }

            //Upwards
            if ((game->game_map[i][j].shape == game->game_map[i][j - 1].shape &&
                 game->game_map[i][j].shape == game->game_map[i][j - 2].shape) &&
                (game->game_map[i][j].active == TRUE)) {
                //Moving down
                //Shift down once k times
                for (k = MAX_BALL_SPAWN - 1; k >= 0; k--) {
                    shiftOneDown(game, i, j);
                }
                //And giving points
                game->points++;
            }

            //Diagonals
            //Right
            if ((game->game_map[i][j].shape == game->game_map[i + 1][j - 1].shape &&
                 game->game_map[i][j].shape == game->game_map[i + 2][j - 2].shape) &&
                (game->game_map[i][j].active == TRUE)) {

                shiftOneDown(game, i, j);
                shiftOneDown(game, (i + 1), (j - 1));
                shiftOneDown(game, (i + 2), (j - 2));

                game->points++;
            }

            if ((game->game_map[i][j].shape == game->game_map[i + 1][j + 1].shape &&
                 game->game_map[i][j].shape == game->game_map[i + 2][j + 2].shape) &&
                (game->game_map[i][j].active == TRUE)) {

                shiftOneDown(game, i, j);
                shiftOneDown(game, (i + 1), (j + 1));
                shiftOneDown(game, (i + 2), (j + 2));

                game->points++;
            }
            // Left
            if ((game->game_map[i][j].shape == game->game_map[i - 1][j - 1].shape &&
                 game->game_map[i][j].shape == game->game_map[i - 2][j - 2].shape) &&
                (game->game_map[i][j].active == TRUE)) {

                shiftOneDown(game, i, j);
                shiftOneDown(game, (i - 1), (j - 1));
                shiftOneDown(game, (i - 2), (j - 2));

                game->points = game->points + 3;
            }
            if ((game->game_map[i][j].shape == game->game_map[i - 1][j + 1].shape &&
                 game->game_map[i][j].shape == game->game_map[i - 2][j + 2].shape) &&
                (game->game_map[i][j].active == TRUE)) {

                shiftOneDown(game, i, j);
                shiftOneDown(game, (i - 1), (j + 1));
                shiftOneDown(game, (i - 2), (j + 2));

                game->points++;
            }
        }
    }
}

/*******************************************************************
 * @objectives checking if value is occupied, if not moves the moving
 * element left.
 *
 * @param game->focused_x[2], game->focused_x[1], game->focused_x[0]
 */
void navLeft(struct Games *game) {
    if ((game->game_map[game->focused_x[2] - 1][game->focused_y[2]].active == TRUE &&
            game->game_map[game->focused_x[1] - 1][game->focused_y[1]].active == TRUE &&
            game->game_map[game->focused_x[0] - 1][game->focused_y[0]].active == TRUE) || game->focused_x[2] <= 0) {
        game->focused_x[2] = 0;
        game->focused_x[1] = 0;
        game->focused_x[0] = 0;
    }
    else {
        //First figure
        game->game_map[game->focused_x[2] - 1][game->focused_y[2]].active =
                game->game_map[game->focused_x[2]][game->focused_y[2]].active;
        game->game_map[game->focused_x[2] - 1][game->focused_y[2]].shape =
                game->game_map[game->focused_x[2]][game->focused_y[2]].shape;

        game->game_map[game->focused_x[2]][game->focused_y[2]].active = FALSE;
        game->focused_x[2]--;

        //Secound figure
        game->game_map[game->focused_x[1] - 1][game->focused_y[1]].active =
                game->game_map[game->focused_x[1]][game->focused_y[1]].active;
        game->game_map[game->focused_x[1] - 1][game->focused_y[1]].shape =
                game->game_map[game->focused_x[1]][game->focused_y[1]].shape;

        game->game_map[game->focused_x[1]][game->focused_y[1]].active = FALSE;
        game->focused_x[1]--;

        //Third figure
        game->game_map[game->focused_x[0] - 1][game->focused_y[0]].active =
                game->game_map[game->focused_x[0]][game->focused_y[0]].active;
        game->game_map[game->focused_x[0] - 1][game->focused_y[0]].shape =
                game->game_map[game->focused_x[0]][game->focused_y[0]].shape;

        //Remove last
        game->game_map[game->focused_x[0]][game->focused_y[0]].active = FALSE;
        game->focused_x[0]--;

    }
}

/*******************************************************************
 * @objectives checking if value is occupied,if it is, run procedures
 * of locking element, cheking for links and spawning new element
 * ..if not moves the down.
 *
 * @param game->focused_x[2], game->focused_x[1], game->focused_x[0]
 */
void navDown(struct Games *game) {
        /*Checking if the block under is occupied*/
        if (game->game_map[game->focused_x[2]][game->focused_y[2] + 1].active == TRUE ||
        game->focused_y[2] == MAP_HEIGHT_SQUARES - 3) {
            if (game->game_map[MAP_WIDTH_SQUARES/2][FIGURE_SPAWN_3].active == TRUE) {
                game->gameOver = TRUE;
            }
            else {
                checkPoints(game);
                spawnFigures(game);
            }
        }
        else {
            //First figure
            game->game_map[game->focused_x[2]][game->focused_y[2] + 1].active =
                    game->game_map[game->focused_x[2]][game->focused_y[2]].active;
            game->game_map[game->focused_x[2]][game->focused_y[2] + 1].shape =
                    game->game_map[game->focused_x[2]][game->focused_y[2]].shape;
            game->focused_y[2]++;

            //Secound figure
            game->game_map[game->focused_x[1]][game->focused_y[1] + 1].active =
                    game->game_map[game->focused_x[1]][game->focused_y[1]].active;
            game->game_map[game->focused_x[1]][game->focused_y[1] + 1].shape =
                    game->game_map[game->focused_x[1]][game->focused_y[1]].shape;
            game->focused_y[1]++;

            //Third figure
            game->game_map[game->focused_x[0]][game->focused_y[0] + 1].active =
                    game->game_map[game->focused_x[0]][game->focused_y[0]].active;
            game->game_map[game->focused_x[0]][game->focused_y[0] + 1].shape =
                    game->game_map[game->focused_x[0]][game->focused_y[0]].shape;

            //Remove last
            game->game_map[game->focused_x[0]][game->focused_y[0]].active = FALSE;
            game->focused_y[0]++;

        }

}

/*******************************************************************
 * @objectives checking if value is occupied, if not moves the moving
 * element right.
 *
 * @param game->focused_x[2], game->focused_x[1], game->focused_x[0]
 */
void navRight(struct Games *game) {

    if ((game->game_map[game->focused_x[2] + 1][game->focused_y[2]].active == TRUE &&
            game->game_map[game->focused_x[1] + 1][game->focused_y[1]].active == TRUE &&
            game->game_map[game->focused_x[0] + 1][game->focused_y[0]].active == TRUE) ||
            game->focused_x[2] == MAP_WIDTH_SQUARES - 1) {

    }
    else {
        //First figure
        game->game_map[game->focused_x[2] + 1][game->focused_y[2]].active =
                game->game_map[game->focused_x[2]][game->focused_y[2]].active;
        game->game_map[game->focused_x[2] + 1][game->focused_y[2]].shape =
                game->game_map[game->focused_x[2]][game->focused_y[2]].shape;

        game->game_map[game->focused_x[2]][game->focused_y[2]].active = FALSE;
        game->focused_x[2]++;

        //Secound figure
        game->game_map[game->focused_x[1] + 1][game->focused_y[1]].active =
                game->game_map[game->focused_x[1]][game->focused_y[1]].active;
        game->game_map[game->focused_x[1] + 1][game->focused_y[1]].shape =
                game->game_map[game->focused_x[1]][game->focused_y[1]].shape;

        game->game_map[game->focused_x[1]][game->focused_y[1]].active = FALSE;
        game->focused_x[1]++;

        //Third figure
        game->game_map[game->focused_x[0] + 1][game->focused_y[0]].active =
                game->game_map[game->focused_x[0]][game->focused_y[0]].active;
        game->game_map[game->focused_x[0] + 1][game->focused_y[0]].shape =
                game->game_map[game->focused_x[0]][game->focused_y[0]].shape;

        //Remove last
        game->game_map[game->focused_x[0]][game->focused_y[0]].active = FALSE;
        game->focused_x[0]++;

    }
}

/*******************************************************************
 * @objectives flipping the different shape values of the moving figures
 *
 * @param game->focused_x[2], game->focused_x[1], game->focused_x[0]
 */
void changeJewels(struct Games *game) {
    Boxes tmp;
    tmp.shape = game->game_map[game->focused_x[2]][game->focused_y[2]].shape;
    game->game_map[game->focused_x[2]][game->focused_y[2]].shape = game->game_map[game->focused_x[1]][game->focused_y[1]].shape;
    game->game_map[game->focused_x[1]][game->focused_y[1]].shape = game->game_map[game->focused_x[0]][game->focused_y[0]].shape;
    game->game_map[game->focused_x[0]][game->focused_y[0]].shape = tmp.shape;
}

/*******************************************************************
 * @objectives looping through the gameboard matrice, reseting
 * all the values shapes and activity variables.
 * then calling for the prcedure that initiate the spawning of a new
 * set of figures (jewels).
 *
 * @param *game
 */
void resetGame(struct Games *game) {
    int i, j;
    game->points = 0;
    game->time.seconds = 0;
    game->time.minutes = 0;
    game->time.hours = 0;

    for (i = 0; i < MAP_WIDTH_SQUARES; i++) {
        for (j = 0; j < MAP_HEIGHT_SQUARES; j++) {
            game->game_map[i][j].shape = FIGURE_NONE;
            game->game_map[i][j].active = FALSE;
        }
    }
    spawnFigures(game);
}

/*******************************************************************
 * @objectives writes the content of the main struct of this game
 * into a binary file
 *
 * @param game->focused_x[2], game->focused_x[1], game->focused_x[0]
 */
void saveGame(struct Games *game) {
    FILE *file;

    // open file for writing
    file = fopen ("game.bin", "wb");
    if (file == NULL)
    {
        printf("\nError opening file\n");
    }
    else {
    // write struct to file
        fwrite (game, sizeof(struct Games), 1, file);

        if(fwrite != 0) {
            printf("\n Username: %s. Points: %d.\nGame saved successfully.\n", game->username.string, game->points);
        }
        else {
        
            printf("\n\nERROR: Game for username: %s not saved..\n", game->username.string);
        }
        fclose(file);
    
    }

}

/*******************************************************************
 * @objectives handles the counting of gametime functionality
 *
 * @param game->time
 */
void updateGameTime(struct Games *game) {
    game->time.seconds++;
    if (game->time.seconds >= 60) {
            if (game->time.seconds >= 120) {
                game->time.minutes += 2;
            }
            else {
                game->time.seconds = 0;
                game->time.minutes++;
            }
        }
        else {
            if (game->time.minutes >= 60) {
                game->time.minutes = 0;
                game->time.hours++;
            }
        }
}

/*******************************************************************
 * @objectives This procedure handles the main functionality of the game.
 * This is where we find the mainloop constantly updating the slave
 * window. It is also here we find all the different calls for functionality
 * procedures related to game interaction, such as keystrokes/keybinds.
 *
 * @param time1, time2, nSortir
 */
void gameController(struct Games *game, int initiated) {
    int nSortir = 0;
    float time1,time0;
    int times = 0;
    game->gameOver = FALSE;

    time0 = (float) clock();


    //Initializing Allegro
    LS_allegro_init(SCREEN_WIDTH_PIXELS, SCREEN_HEIGHT_PIXELS,"JewelHunt");

    //Infinite loop for the game
    while(!nSortir){
        if (game->gameOver == TRUE){
            //Handle gameOver
            saveRankings(game);
            initiated = FALSE;
            LS_allegro_clear_and_paint(BLACK);
            game->gameOver = FALSE;
        }
        else {
            if (initiated) {
                time1 = (float) clock();
                if ((time1 - time0) / (float) CLOCKS_PER_SEC >= 0.5) {
                    times++;
                    time0 = (float) clock();
                }
                if (times >= 2) {
                    //1 secound has passed
                    navDown(game);
                    updateGameTime(game);
                    times = 0;
                }
            }
            //Waiting for the ESC key to be pressed
            if (LS_allegro_key_pressed(ALLEGRO_KEY_ESCAPE)) {
                nSortir = 1;
                initiated = FALSE;
            }

            //Waiting for the SAVE(S) key to be pressed
            if (LS_allegro_key_pressed(ALLEGRO_KEY_S)) {
                saveGame(game);
            }
            //Waiting for the ENTER key to be pressed
            if (LS_allegro_key_pressed(ALLEGRO_KEY_ENTER)) {
                if (!initiated) {
                    initiated = TRUE;
                    game->next_figures[0].shape = randomizeSpawn();
                    game->next_figures[1].shape = randomizeSpawn();
                    game->next_figures[2].shape = randomizeSpawn();
                    resetGame(game);
                }

            }


            if (LS_allegro_key_pressed(ALLEGRO_KEY_R)) {
                resetGame(game);
            }

            if (LS_allegro_key_pressed(ALLEGRO_KEY_LEFT)) {
                navLeft(game);
            }

            if (LS_allegro_key_pressed(ALLEGRO_KEY_RIGHT)) {
                navRight(game);
            }
            if (LS_allegro_key_pressed(ALLEGRO_KEY_DOWN)) {
                navDown(game);
            }
            if (LS_allegro_key_pressed(ALLEGRO_KEY_SPACE)) {
                changeJewels(game);
            }

            if(FALSE == initiated) {
                al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), (SCREEN_WIDTH_PIXELS/(float) 2),200,ALLEGRO_ALIGN_CENTER,"%s","Welcome to JewelHunt! .. Press ESC to exit");
                al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE), (SCREEN_WIDTH_PIXELS/(float) 2),300,ALLEGRO_ALIGN_CENTER,"%s","PRESS ENTER TO BEGIN");
            //Restart feature
             }

            //reset graphics & paint black
            LS_allegro_clear_and_paint(BLACK);

            if (TRUE == initiated) {
                printWindowGraphics(game);
                printGameGraphics(game);
            }
        }
    }


    //Closing the graphical window
    LS_allegro_exit();

}

/*******************************************************************
 * @objectives Load binary file to restore all the memory data the
 * user had when he or she saved their game.
 *
 * @param f
 */
void loadGame(struct Games *game) {
    FILE *f;
    f = fopen("game.bin", "rb");
    if (NULL == f) {
        printf("\n\nThe file is empty..\n");
    }
    else {
        printf("\n\nLoading file...");
        while (fread(game, sizeof(struct Games), 1, f));
        fclose(f);
        gameController(game, TRUE);
    }
}

/*******************************************************************
 * @objectives This function first ask for a user input through the
 * keyboard to later set the representing values into the string
 * struct, and then later returning this struct.
 *
 * @param user_input
 */
String getString () {
    /*local variable declaration and initialzation*/
    String user_input;
    user_input.length = 0;

    fgets(user_input.string, sizeof(user_input.string), stdin);                                //getting user input from stdin

    user_input.length = stringLength(user_input.string);

    return user_input;                                                                  //returning user input string
}

/*******************************************************************
 * @objectives checks whether the Input string is a valid input.
 *
 * @param valid_input, input, state
 * @return valid_input
 */
int validateConsoleInput(int state, const String input) {
    int valid_input = FALSE;

    //Checking validity of single character input
    if (input.length == 1) {
        switch (input.string[0]) {
            case '1':
            case '2':
            case '3':
                valid_input = TRUE;
                break;

            default:
                break;
        }
    } else {
        //Main-menu
        if (STATE_MAIN_MENU == state) {

            //Checking validity of full string input
            if (strcmp(input.string, "Play game\n") == 0) valid_input = TRUE;
            if (strcmp(input.string, "Show ranking\n") == 0) valid_input = TRUE;
            if (strcmp(input.string, MENU_EXIT_STRING) == 10 || strcmp(input.string, MENU_EXIT_STRING) == 0) valid_input = TRUE;
        }
        //Play game-menu
        if (STATE_PLAY_MENU == state) {
            //Checking validity of full string input
            if (strcmp(input.string, "New game\n") == 0) valid_input = TRUE;
            if (strcmp(input.string, "Load game\n") == 0) valid_input = TRUE;
            if (strcmp(input.string, MENU_EXIT_STRING) == 10 || strcmp(input.string, MENU_EXIT_STRING) == 0) valid_input = TRUE;
        }
    }
    //Validating input of Play game-menu
    if (2 == state) {

    }
    return valid_input;
}

/*******************************************************************
 * @objectives This procedure recieves a string then later to
 * evaluate if the input is a valid one.
 *
 * @param error, game->username
 */
void getUsername(struct Games *game) {
    int i, error = TRUE;
    do {
        printf("\n\nUsername: ");
        game->username = getString();
        for (i = 0; i < game->username.length; i++) {
            if (game->username.string[i] == '-') {
                error = TRUE;
                i = game->username.length;
            }
            else {
                error = false;
            }
        }
        if (error == TRUE) {
            printf("\n\nERROR:   Invalid character in username (-)");
        }
    }while(error);
    game->username.string[game->username.length] = '\0'; /*Add backlash zero at the end of the filled character arrays
                                                          to signify that the string is finished*/

}

/*******************************************************************
 * @objectives This procedure handles the different console inputs
 * related to the console menu, both setting assigned values as a
 * response or calling for the corresponding procedures related
 * to the console input.
 *
 * @param *state, input, *game
 */
void consoleInputHandler(struct Games *game, int *state, const String input) {
    int i;
    if (STATE_MAIN_MENU == *state) {
        if (input.length == 1) {
            switch (input.string[0]) {
                case '1': //Play game
                    *state = STATE_PLAY_MENU;
                    break;

                case '2': //Show rankings

                    *state = STATE_SHOW_RANKINGS;
                    break;

                case '3': //Exit
                    if (*state > 1) *state = 1;
                    else {
                        *state = STATE_EXIT;
                    }
                    break;

                default:
                    break;
            }
        } else {
            //Checking validity of full string input
            if (strcmp(input.string, "Play game\n") == 0) {
                *state = STATE_PLAY_MENU;
            }
            if (strcmp(input.string, "Show ranking\n") == 0) {
                *state = STATE_SHOW_RANKINGS;
            }
            if (strcmp(input.string, MENU_EXIT_STRING) == 10 || strcmp(input.string, MENU_EXIT_STRING) == 0) {
                *state = STATE_EXIT;

            }
        }
    }
    else {
        //Play game-menu
        if (STATE_PLAY_MENU == *state) {
            if (input.length == 1) {
                switch (input.string[0]) {
                    case '1':
                        getUsername(game);
                        gameController(game, FALSE);
                        *state = STATE_MAIN_MENU;
                        break;

                    case '2':
                        loadGame(game);
                        *state = STATE_PLAY_MENU;
                        printf("\n\nGame loaded..\n\n");
                        break;

                    case '3':
                        *state = STATE_MAIN_MENU;
                        break;

                    default:
                        break;
                }
            } else {
                //Checking validity of full string input
                if (strcmp(input.string, "New game\n") == 0) {
                    getUsername(game);

                    gameController(game, FALSE);
                    *state = STATE_MAIN_MENU;
                }
                    
                if (strcmp(input.string, "Load game") == 0) {
                    loadGame(game);
                    *state = STATE_PLAY_MENU;
                    printf("\n\nGame loaded..\n\n");
                }
                if (strcmp(input.string, MENU_EXIT_STRING) == 10 || strcmp(input.string, MENU_EXIT_STRING) == 0) {
                    *state = STATE_MAIN_MENU;

                }
            }
        }
    }
}

/*******************************************************************
 * @objectives This procedure prints the different headers depending
 * on the consolewindow's state.
 *
 * @param state
 */
void console_menu_header(int state) {
    if (1 == state) printf("---------- JEWELS ----------\n");
    if (2 == state) printf("-------- PLAY GAME --------\n");
    if (3 == state) printf("--------- RANKING ---------\n");
}

/*******************************************************************
 * @objectives Printing the corresponding menu content to the state
 * that the console has. Also handling different procedure calls
 * to handle the functionality of the console menu.
 *
 * @param valid_input, *game, *state, input
 */
void console_menu_content(struct Games *game, int * state) {
    int valid_input = FALSE;
    String input;
    if (STATE_MAIN_MENU == *state) {
        while (valid_input == FALSE) {
            printf("1. Play game\n"
                   "2. Show rankings\n"
                   "3. %s\n"
                   "Option: ", MENU_EXIT_STRING);
            input = getString();
            valid_input = validateConsoleInput(*state, input);
            if (valid_input == TRUE) {
                consoleInputHandler(game, state, input);
            } else {
                printf("ERROR:   Invalid option.\n");
            }
        }
    }

    if (STATE_PLAY_MENU == *state) {
        while (valid_input == FALSE) {
            printf("1. New game\n"
                   "2. Load game\n"
                   "3. %s\n"
                   "Option: ", MENU_EXIT_STRING);
            input = getString();
            valid_input = validateConsoleInput(*state, input);
            if (valid_input == TRUE) {
                consoleInputHandler(game, state, input);
            } else {
                printf("ERROR:   Invalid option.\n");
            }
        }
    }
    if (STATE_SHOW_RANKINGS == *state) {
        loadRankings(game);
        printConsoleRankings(game);
        *state = 1;
    }
}

/*******************************************************************
 * @objectives Handling the main calls for procedures to initiate
 * the program functionality and also handles the exiting of the
 * program.
 *
 * @param state, exit_trigger
 */
void console_menu(struct Games *game) {
    int state = STATE_MAIN_MENU;
    int exit_trigger = FALSE;
    game->time.hours = 0;
    game->time.minutes = 0;
    game->time.seconds = 0;

    checkRankingsFile();
    do {
        console_menu_header(state);
        console_menu_content(game, &state);

        if (STATE_EXIT == state) exit_trigger = TRUE;
    } while (exit_trigger == FALSE);
}

/*******************************************************************
 * @objectives This function's purpose is to initiate the core
 * functionalities of the program and also initiating the content
 * flow.
 *
 * @param game
 */
int main(void){
    struct Games game;
    console_menu(&game);
    return 0;
}
